import Vue from 'vue'
import App from './App.vue'

import Slider from './components/Slider.vue';
import Title from './components/Title.vue';

import Timeline from './components/Timeline.vue';
import MainImage from './components/MainImage.vue';

import Language from './components/Language.vue';
import Social from './components/Social.vue';

Vue.config.productionTip = false

Vue.component('slider',Slider);
Vue.component('slider-title',Title);
Vue.component('timeline',Timeline);
Vue.component('language',Language);
Vue.component('mainimage',MainImage);
Vue.component('social',Social);

new Vue({
  render: h => h(App),
}).$mount('#app')
